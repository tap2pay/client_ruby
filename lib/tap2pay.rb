# frozen_string_literal: true

require "tap2pay/version"
require "tap2pay/errors"
require "tap2pay/client"

module Tap2pay
  autoload :ObjectifiedHash, "tap2pay/objectified_hash"
end
