# frozen_string_literal: true

require "faraday"
require "faraday_middleware"
require_relative "./invoices"
require_relative "./subscriptions"

module Tap2pay
  class Client
    class ErrorHandlingMiddleware < Faraday::Middleware
      def call(env)
        @app.call(env).on_complete do |response_env|
          next if response_env.status < 300

          case response_env.status
          when 300..399
            raise UnknownResponseError
          when 401, 403
            raise AuthorizationError
          when 404
            raise NotFoundError
          when 422
            raise ValidationError.new(nil, response_env.body)
          else raise ServerError
          end
        end
      end
    end

    include Invoices
    include Subscriptions

    attr_reader :api_token, :connection

    def initialize(api_token, base_url: "https://secure.tap2pay.me/")
      @api_token = api_token
      @connection = Faraday.new(base_url) do |c|
        c.headers["Authorization"] = "Bearer #{api_token}"

        c.use ErrorHandlingMiddleware

        c.request :json
        c.response :json, content_type: /\bjson$/

        c.adapter Faraday.default_adapter
      end
    end

    protected

    def wrap_response
      resp = yield

      body = resp&.body
      body = ObjectifiedHash.new(body) if body

      body
    rescue Faraday::Error => e
      raise ConnectionError, e
    end
  end
end
