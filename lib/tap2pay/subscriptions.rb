# frozen_string_literal: true

module Tap2pay
  module Subscriptions
    def subscriptions(page: nil, per_page: nil)
      wrap_response do
        connection.get("/api/subscriptions/") do |req|
          req.params = { page: page, per_page: per_page }.reject { |_k, v| v.nil? }
        end
      end
    end

    def subscription(id)
      wrap_response do
        connection.get("/api/subscriptions/#{id}")
      end
    end

    def create_subscription(subscription)
      wrap_response do
        connection.post("/api/subscriptions") do |req|
          req.body = {
            subscription: subscription,
          }.to_json
        end
      end
    end

    def update_subscription(id, subscription)
      wrap_response do
        connection.put("/api/subscriptions/#{id}") do |req|
          req.body = {
            subscription: subscription,
          }.to_json
        end
      end
    end

    def delete_subscription(id)
      wrap_response do
        connection.delete("/api/subscriptions/#{id}")
      end
    end
  end
end
