# frozen_string_literal: true

module Tap2pay
  class Error < StandardError; end
  class AuthorizationError < Error; end
  class ServerError < Error; end
  class NotFoundError < Error; end
  class UnknownResponseError < Error; end
  class ConnectionError < Error; end

  class ValidationError < Error
    attr_reader :errors

    def initialize(msg, errors = nil)
      @errors = errors

      super(msg)
    end
  end
end
