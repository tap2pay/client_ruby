# frozen_string_literal: true

module Tap2pay
  module Invoices
    def invoices(page: nil, per_page: nil)
      wrap_response do
        connection.get("/api/invoices/") do |req|
          req.params = { page: page, per_page: per_page }.reject { |_k, v| v.nil? }
        end
      end
    end

    def invoice(id)
      wrap_response do
        connection.get("/api/invoices/#{id}")
      end
    end

    def create_invoice(invoice)
      wrap_response do
        connection.post("/api/invoices") do |req|
          req.body = {
            invoice: invoice,
          }.to_json
        end
      end
    end

    def update_invoice(id, invoice)
      wrap_response do
        connection.put("/api/invoices/#{id}") do |req|
          req.body = {
            invoice: invoice,
          }.to_json
        end
      end
    end

    def delete_invoice(id)
      wrap_response do
        connection.delete("/api/invoices/#{id}")
      end
    end
  end
end
