# frozen_string_literal: true

RSpec.describe Tap2pay do
  it "has a version number" do
    expect(Tap2pay::VERSION).not_to be nil
  end
end
