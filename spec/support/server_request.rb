# frozen_string_literal: true

require "spec_helper"

RSpec.shared_examples "server request" do
  it "raises not found error" do
    stubbed_request.to_return(status: 404)

    expect { subject }.to raise_error(Tap2pay::NotFoundError)
  end

  it "raises connection error on timeout" do
    stubbed_request.to_timeout

    expect { subject }.to raise_error(Tap2pay::ConnectionError)
  end

  it "raises server error" do
    stubbed_request.to_return(status: 500)

    expect { subject }.to raise_error(Tap2pay::ServerError)
  end
end
