# frozen_string_literal: true

require "spec_helper"

RSpec.describe Tap2pay::Invoices do
  let(:client) { Tap2pay::Client.new("asd") }
  let(:invoice_params) do
    {
      items: [
        {
          title: "Test",
          price_value: 200,
        },
      ],
    }
  end
  let(:invoice_response) do
    {
      amount: "2.00$",
      amount_value: 200,
      amount_currency: "USD",
      items: [
        {
          title: "Test",
          price_value: 200,
        },
      ],
    }
  end

  describe "#invoices" do
    subject { client.invoices }
    let(:stubbed_request) { stub_request(:get, "https://secure.tap2pay.me/api/invoices/") }

    it_behaves_like "server request"

    it "responds with invoice" do
      stubbed_request.to_return(
        body: { items: [invoice_response] }.to_json,
        status: 200,
        headers: { content_type: "application/json" }
      )

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end
  end

  describe "#invoice" do
    subject { client.invoice(123) }
    let(:stubbed_request) { stub_request(:get, "https://secure.tap2pay.me/api/invoices/123") }

    it_behaves_like "server request"

    it "responds with invoice" do
      stubbed_request.to_return(
        body: invoice_response.to_json,
        status: 200,
        headers: { content_type: "application/json" }
      )

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end
  end

  describe "#create_invoice" do
    subject { client.create_invoice(invoice_params) }
    let(:stubbed_request) { stub_request(:post, "https://secure.tap2pay.me/api/invoices") }

    it_behaves_like "server request"

    it "responds with invoice" do
      stubbed_request.to_return(body: invoice_response.to_json,
                                status: 200,
                                headers: { content_type: "application/json" })

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end

    it "raise validation error" do
      stubbed_request.to_return(body: { errors: ["error"] }.to_json,
                                status: 422,
                                headers: { content_type: "application/json" })

      expect { subject }.to raise_error(Tap2pay::ValidationError)
    end
  end

  describe "#update" do
    subject { client.update_invoice(123, invoice_params) }
    let(:stubbed_request) { stub_request(:put, "https://secure.tap2pay.me/api/invoices/123") }

    it_behaves_like "server request"

    it "responds with invoice" do
      stubbed_request.to_return(body: invoice_response.to_json,
                                status: 200,
                                headers: { content_type: "application/json" })

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end

    it "raise validation error" do
      stubbed_request.to_return(body: { errors: ["error"] }.to_json,
                                status: 422,
                                headers: { content_type: "application/json" })

      expect { subject }.to raise_error(Tap2pay::ValidationError)
    end
  end

  describe "#destroy" do
    subject { client.delete_invoice(123) }
    let(:stubbed_request) { stub_request(:delete, "https://secure.tap2pay.me/api/invoices/123") }

    it_behaves_like "server request"

    it "responds with true" do
      stubbed_request.to_return(status: 200,
                                headers: { content_type: "application/json" })

      is_expected.to be_nil
    end
  end
end
