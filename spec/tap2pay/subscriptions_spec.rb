# frozen_string_literal: true

require "spec_helper"

RSpec.describe Tap2pay::Subscriptions do
  let(:client) { Tap2pay::Client.new("asd") }
  let(:subscription_params) do
    {
      title: "Subscription",
      amount_value: 200,
    }
  end
  let(:subscription_response) do
    {
      title: "Subscription",
      amount: "2.00$",
      amount_value: 200,
      amount_currency: "USD",
    }
  end

  describe "#subscriptions" do
    subject { client.subscriptions }
    let(:stubbed_request) { stub_request(:get, "https://secure.tap2pay.me/api/subscriptions/") }

    it_behaves_like "server request"

    it "responds with subscriptions" do
      stubbed_request.to_return(
        body: { items: [subscription_response] }.to_json,
        status: 200,
        headers: { content_type: "application/json" }
      )

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end
  end

  describe "#subscription" do
    subject { client.subscription(123) }
    let(:stubbed_request) { stub_request(:get, "https://secure.tap2pay.me/api/subscriptions/123") }

    it_behaves_like "server request"

    it "responds with invoice" do
      stubbed_request.to_return(
        body: subscription_response.to_json,
        status: 200,
        headers: { content_type: "application/json" }
      )

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end
  end

  describe "#create_subscription" do
    subject { client.create_subscription(subscription_params) }
    let(:stubbed_request) { stub_request(:post, "https://secure.tap2pay.me/api/subscriptions") }

    it_behaves_like "server request"

    it "responds with subscription" do
      stubbed_request.to_return(body: subscription_response.to_json,
                                status: 200,
                                headers: { content_type: "application/json" })

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end

    it "raise validation error" do
      stubbed_request.to_return(body: { errors: ["error"] }.to_json,
                                status: 422,
                                headers: { content_type: "application/json" })

      expect { subject }.to raise_error(Tap2pay::ValidationError)
    end
  end

  describe "#update" do
    subject { client.update_subscription(123, subscription_params) }
    let(:stubbed_request) { stub_request(:put, "https://secure.tap2pay.me/api/subscriptions/123") }

    it_behaves_like "server request"

    it "responds with subscription" do
      stubbed_request.to_return(body: subscription_response.to_json,
                                status: 200,
                                headers: { content_type: "application/json" })

      is_expected.to be_a(Tap2pay::ObjectifiedHash)
    end

    it "raise validation error" do
      stubbed_request.to_return(body: { errors: ["error"] }.to_json,
                                status: 422,
                                headers: { content_type: "application/json" })

      expect { subject }.to raise_error(Tap2pay::ValidationError)
    end
  end

  describe "#destroy" do
    subject { client.delete_subscription(123) }
    let(:stubbed_request) { stub_request(:delete, "https://secure.tap2pay.me/api/subscriptions/123") }

    it_behaves_like "server request"

    it "responds with true" do
      stubbed_request.to_return(status: 200,
                                headers: { content_type: "application/json" })

      is_expected.to be_nil
    end
  end
end
